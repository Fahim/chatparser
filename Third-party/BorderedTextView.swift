//
//  BorderedTextView.swift
//  Swift Tools
//
//  Created by Fahim Farook on 17/01/17.
//  Copyright (c) 2014 RookSoft Pte. Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class BorderedTextView:UITextView {
	@IBInspectable var lineWidth:CGFloat = 1.0 {
		didSet {
			setNeedsDisplay()
		}
	}
	
	@IBInspectable var radius:CGFloat = 10.0 {
		didSet {
			setNeedsDisplay()
		}
	}
	
	@IBInspectable var bgColor:UIColor = UIColor.clear {
		didSet {
			setNeedsDisplay()
		}
	}
	
	@IBInspectable var borderColor:UIColor = UIColor.gray {
		didSet {
			setNeedsDisplay()
		}
	}
	
	override func draw(_ rect:CGRect) {
		// Start drawing
		guard let ctx = UIGraphicsGetCurrentContext() else { return }
		// Border rectangle
		let r = CGRect(x:rect.origin.x + lineWidth, y:rect.origin.y + lineWidth, width:rect.size.width - (lineWidth * 2), height:rect.size.height - (lineWidth * 2))
		ctx.setAllowsAntialiasing(true)
		ctx.setLineWidth(lineWidth)
		ctx.setFillColor(bgColor.cgColor)
		ctx.setStrokeColor(borderColor.cgColor)
		// Create rounded rectangles
		ctx.move(to:CGPoint(x:r.minX, y:r.midY))
		ctx.addArc(tangent1End:CGPoint(x:r.minX, y:r.minY), tangent2End:CGPoint(x:r.midX, y: r.minY), radius:radius)
		ctx.addArc(tangent1End:CGPoint(x:r.maxX, y:r.minY), tangent2End:CGPoint(x:r.maxX, y:r.midY), radius:radius)
		ctx.addArc(tangent1End:CGPoint(x:r.maxX, y:r.maxY), tangent2End:CGPoint(x:r.midX, y:r.maxY), radius:radius)
		ctx.addArc(tangent1End:CGPoint(x:r.minX, y:r.maxY), tangent2End:CGPoint(x:r.minX, y:r.midY), radius:radius)
		ctx.closePath()
		ctx.drawPath(using:CGPathDrawingMode.fillStroke)
	}	
}
