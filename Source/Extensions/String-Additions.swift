//
//  String-Additions.swift
//  HipChatParser
//
//  Created by Fahim Farook on 7/3/18.
//  Copyright © 2018 RookSoft Ltd. All rights reserved.
//

import Foundation

extension String {
	func json() -> [String: Any]?  {
		if let data = data(using: String.Encoding.utf8) {
			do {
				return try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]
			} catch {
				NSLog("Error parsing JSON: \(error.localizedDescription)")
				return nil
			}
		} else {
			return nil
		}
	}
}
