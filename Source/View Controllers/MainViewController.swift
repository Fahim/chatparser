//
//  MainViewController.swift
//  HipChatParser
//
//  Created by Fahim Farook on 7/3/18.
//  Copyright © 2018 RookSoft Ltd. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
	@IBOutlet private weak var txtMessage: BorderedTextView!
	@IBOutlet private weak var txtJSON: BorderedTextView!

	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		txtMessage.becomeFirstResponder()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK:- Actions
	@IBAction func parse() {
		txtMessage.resignFirstResponder()
		// Validate
		if txtMessage.text.isEmpty {
			let alert = UIAlertController(title: "Error", message: "No message string entered. Please enter a message string!", preferredStyle: UIAlertControllerStyle.alert)
			alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
			present(alert, animated: true, completion: nil)
			return
		}
		// Parse text
		let parser = MessageParser()
		txtJSON.text = parser.parse(message: txtMessage.text)
	}
}
