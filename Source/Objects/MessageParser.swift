//
//  MessageParser.swift
//  HipChatParser
//
//  Created by Fahim Farook on 7/3/18.
//  Copyright © 2018 RookSoft Ltd. All rights reserved.
//

import Foundation

class MessageParser {
	// MARK:- Public Methods
	func parse(message: String) -> String {
		var json = "{"
		var hasContent = false
		// Mentions
		let mentions = getMentions(message: message)
		if mentions.count > 0 {
			hasContent = true
			json += "\"mentions\": ["
			let buf = mentions.reduce("", {(res, val) in
				res.isEmpty ? "\"\(val)\"" : res + ", " + "\"\(val)\""
			})
			json += buf + "]"
		}
		// Emoticons
		let emos = getEmoticons(message: message)
		if emos.count > 0 {
			if hasContent {
				json += ", \n"
			}
			hasContent = true
			json += "\"emoticons\": ["
			let buf = emos.reduce("", {(res, val) in
				res.isEmpty ? "\"\(val)\"" : res + ", " + "\"\(val)\""
			})
			json += buf + "]"
		}
		// Links
		let links = getLinks(message: message)
		if links.count > 0 {
			if hasContent {
				json += ", \n"
			}
			hasContent = true
			json += "\"links\": ["
			let buf = links.reduce("", {(res, link) in
				let url = link["url"]!
				let title = link["title"]!
				let row = "{\"url\": \"\(url)\", \"title\": \"\(title)\"}"
				let buf = res.isEmpty ? row : res + ", " + row
				return buf
			})
			json += buf + "]"
		}

		// Close out JSON
		json += "}"
		return json
	}
	
	// MARK:- Private Methods
	private func getMentions(message: String) -> [String] {
		let arr = matches(pattern: "\\b@[a-z0-9]+", text: message)
		let result = arr.map {(row) -> String in
			// Remove initial @
			let start = row.index(after: row.startIndex)
			return String(row[start...])
		}
		return result
	}
	
	private func getEmoticons(message: String) -> [String] {
		let arr = matches(pattern: "\\(\\w+\\)", text: message)
		let result = arr.map {(row) -> String in
			// Remove the brackets
			let start = row.index(after: row.startIndex)
			let end = row.index(before: row.endIndex)
			return String(row[start..<end])
		}
		return result
	}

	private func getLinks(message: String) -> [[String:String]] {
		let arr = matches(pattern: "((http|https)\\:\\/\\/)?[a-z0-9\\-\\.]+\\.[a-z]{2,3}(\\/\\S*)?", text: message)
		let group = DispatchGroup()
		var result = [[String:String]]()
		for row in arr {
			var link = [String:String]()
			var buf = row
			// TODO: Prepending http:// requires disabling ATS. However, going with that since the expected output specified http:// for links without the protocol.
			// Prepend http:// if necessary
			if !buf.hasPrefix("http") {
				buf = "http://" + row
			}
			// Process final URL
			link["url"] = buf
			group.enter()
			getLinkTitle(url: buf, completion: {(title) in
				link["title"] = title
				result.append(link)
				group.leave()
			})
		}
		group.wait()
		return result
	}
	
	private func matches(pattern: String, text: String) -> [String] {
		var result = [String]()
		do {
			let regex = try NSRegularExpression(pattern: pattern, options: [.useUnicodeWordBoundaries, .caseInsensitive])
			let matches = regex.matches(in: text, options: [], range: NSRange(location: 0, length: text.count))
			result = matches.map {
				String(text[Range($0.range, in: text)!])
			}
			return result
		} catch {
			NSLog("Regular Expression error: \(error.localizedDescription)")
		}
		return result
	}
	
	private func getLinkTitle(url: String, completion:@escaping (_ title: String)->Void) {
		DispatchQueue.global().async {
			guard let link = URL(string: url) else {
				completion("")
				return
			}
			do {
				let str = try String.init(contentsOf: link)
				let arr = self.matches(pattern: "<title>.*?</title>", text: str)
				if let buf = arr.first {
					let title = buf.replacingOccurrences(of: "<title>", with: "", options: [.caseInsensitive]).replacingOccurrences(of: "</title>", with: "", options: [.caseInsensitive])
					completion(title)
				} else {
					completion("")
				}
			} catch {
				NSLog("Error getting contents of URL: \(url) - \(error.localizedDescription)")
				completion("")
			}
		}
	}
}
