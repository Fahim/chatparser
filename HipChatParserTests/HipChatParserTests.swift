//
//  HipChatParserTests.swift
//  HipChatParserTests
//
//  Created by Fahim Farook on 7/3/18.
//  Copyright © 2018 RookSoft Ltd. All rights reserved.
//

import XCTest
@testable import HipChatParser

class HipChatParserTests: XCTestCase {
	private var parser = MessageParser()
	
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

	// Basic validation
	func testOuptut() {
		let json = parser.parse(message: "My message")
		XCTAssertFalse(json.isEmpty, "The output from the parser should not be empty!")
	}
	
	// JSON output tests for expected results
	func testMentions() {
		let message = "@chris you around?"
	    // Expected - { "mentions": [ "chris" ] }
		let expected = ["chris"]
		checkMentions(message: message, expected: expected)
	}
	
	func testEmoticons() {
		let message = "Good morning! (megusta) (coffee)"
		// Expected - { "emoticons": [ "megusta", "coffee" ] }
		let expected = ["megusta", "coffee"]
		checkEmoticons(message: message, expected: expected)
	}

	func testLinks() {
		let message = "Olympics are starting soon;www.nbcolympics.com"
		/*
		Expected - 	{ "links": [
		{ "url": "http://www.nbcolympics.com", "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia" }]}
		*/
		// TODO: The originally provided title is no longer there on the site
		let expected = [["url": "http://www.nbcolympics.com", "title": "2018 PyeongChang Olympic Games | NBC Olympics"]]
		checkLinks(message: message, expected: expected)
	}

	func testAll() {
		let message = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
		/*
		Expected - 	{"mentions": ["bob", "john"],
		"emoticons": ["success"],
		"links": [
		{ "url": "https://twitter.com/jdorfman/status/430511497475670016", "title": "Twitter / jdorfman: nice @littlebigdetail from ..." }
		]}
		*/
		// TODO: The provided expected title does not match what is received from the site. Not sure if the expectation is that the full title be displayed or only a portion of the title be displayed. Going with the full title for the moment.
		checkMentions(message: message, expected: ["bob", "john"])
		checkEmoticons(message: message, expected: ["success"])
		checkLinks(message: message, expected: [["url": "https://twitter.com/jdorfman/status/430511497475670016", "title": "Justin Dorfman; on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"]])
	}
	
	func checkMentions(message: String, expected: [String]) {
		let result = parser.parse(message: message)
		guard let json = result.json() else {
			XCTFail("Could not parse result JSON for mentions")
			return
		}
		guard let mentions = json["mentions"] as? [String] else {
			XCTFail("No valid mentions found!")
			return
		}
		XCTAssertEqual(mentions, expected, "Expected mentions not found!")
	}
	
	func checkEmoticons(message: String, expected: [String]) {
		let result = parser.parse(message: message)
		guard let json = result.json() else {
			XCTFail("Could not parse result JSON for emoticons")
			return
		}
		guard let emos = json["emoticons"] as? [String] else {
			XCTFail("No valid emoticons found!")
			return
		}
		XCTAssertEqual(emos, expected, "Expected emoticons not found!")
	}
	
	func checkLinks(message: String, expected: [[String:String]]) {
		let result = parser.parse(message: message)
		guard let json = result.json() else {
			XCTFail("Could not parse result JSON")
			return
		}
		guard let links = json["links"] as? [[String:String]] else {
			XCTFail("No valid links found!")
			return
		}
		XCTAssertEqual(links.count, expected.count, "Links array count does not match expected array count!")
		for (ndx, row) in expected.enumerated() {
			for key in row.keys {
				let val = row[key]
				let jrow = links[ndx]
				guard let jval = jrow[key] else {
					XCTFail("Value for key: \(key) not found in link results")
					return
				}
				XCTAssertEqual(jval, val, "Value for key: \(key) in links is not correct!")
			}
		}
	}
}
